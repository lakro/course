# Maxibooking

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.1.
# # Настройка и установка

1. В качестве package manager в проекте используется npm. Для установки пакетов необходимо в корневой директории проекта вызвать npm install.

2. Запустить компиляцию Angular командой ng serve -o.

3. Установка закончена, удачного тестирования.