
import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpService } from '@app/core/services/http.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit, OnDestroy {

  constructor(private httpSrv: HttpService) { }

  ngOnInit(): void {
    this.httpSrv.start();
  }

  ngOnDestroy(): void {
    this.httpSrv.stop();
  }

  get courseData(): Observable<any> {
    return this.httpSrv.sourceCource$;
  }


}
