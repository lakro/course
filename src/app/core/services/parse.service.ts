import { Injectable } from '@angular/core';
import { Course } from '../models/model';
import * as xml2js from 'xml2js';

const ID_COURSE = 'R01239';

@Injectable({
  providedIn: 'root'
})

export class ParseService {
  apiTier = 0;

  constructor() { }

  parseData(data: any): Course {
    const parseFunctions = [ this.parseApi01, this.parseApi02 ];
    return parseFunctions[this.apiTier](data);
  }

  parseApi01(data: any): Course {
    const find: Course = {
      id: '',
      value: 0,
      name: ''
    };

    xml2js.parseString(data, (err, result) => {
      const findIndex = result.ValCurs.Valute.find((index: any) => {
        if (index.$.ID === ID_COURSE) {
          return index;
        }
      });

      find.id = findIndex.$.ID;
      find.value = findIndex.Value[0];
      find.name = findIndex.Name[0];
    });

    return find;
  }

  parseApi02(data: any): Course {
    const find: Course = {
      id: '',
      value: 0,
      name: ''
    };

    Object.values(JSON.parse(data).Valute).forEach((index: any) => {
      if (index.ID === ID_COURSE) {
        find.id = index.ID;
        find.value = index.Value;
        find.name = index.Name;
      }
    });

    return find;
  }
}
