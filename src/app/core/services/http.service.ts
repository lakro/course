import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ReplaySubject, Subscription, timer } from 'rxjs';
import { catchError, expand, map, retryWhen, switchMap, tap } from 'rxjs/operators';
import { ParseService } from './parse.service';

const INTERVAL = 10000;

@Injectable()
export class HttpService {
  api = [
    'https://www.cbr-xml-daily.ru/daily_utf8.xml',
    'https://www.cbr-xml-daily.ru/daily_json.js'
  ];
  sourceCource$: ReplaySubject<any> = new ReplaySubject<any>(1);
  subscriptionCource$: Subscription | undefined;

  constructor(private http: HttpClient, private parseSrv: ParseService) { }

  getData(): Observable<any> {
    return this.http.get(this.api[this.parseSrv.apiTier], { responseType: 'text' as 'json' }).pipe(
      tap(res => this.sourceCource$.next(this.parseSrv.parseData(res))),
      catchError(error => this.getData())
    );
  }

  start(): Observable<any> {
    if (!this.subscriptionCource$) {
      this.subscriptionCource$ = this.createSourceCource().subscribe();
    }

    return this.sourceCource$;
  }

  private createSourceCource(): Observable<any> {
    return this.getData().pipe(
      expand(() => timer(INTERVAL).pipe(
        switchMap(() => this.getData().pipe(retryWhen(_ => timer(INTERVAL))))),
      )
    );
  }

  stop(): void {
    if (this.subscriptionCource$) {
      this.subscriptionCource$.unsubscribe();
      this.subscriptionCource$ = undefined;
    }
    this.sourceCource$.complete();
  }

}
