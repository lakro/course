import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { httpInterceptorProviders } from './interceptors';
import { HttpService } from './services/http.service';
import { ParseService } from './services/parse.service';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    httpInterceptorProviders,
    HttpService
  ]
})
export class CoreModule { }
