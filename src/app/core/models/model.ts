export interface Course {
  id: string;
  value: number;
  name: string;
}