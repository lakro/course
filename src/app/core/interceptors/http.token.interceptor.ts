import { Injectable } from '@angular/core';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { ParseService } from '../services/parse.service';
import { HttpService } from '../services/http.service';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {

  constructor(private parseSrv: ParseService, private httpSrv: HttpService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError(error => {
        this.parseSrv.apiTier++;
        if (this.parseSrv.apiTier > this.httpSrv.api.length) {
          this.parseSrv.apiTier = 0;
        }

        return observableThrowError(error);
      }),
      map((event: HttpEvent<any>) => event),
    );
  }

}
